package com.zaodei.rxjob;

import org.springframework.boot.builder.SpringApplicationBuilder;

/**
 *@Title:web容器中进行部署
 *@Description:
 *@Author:wuche@laozo.com
 *@time:2019年1月9日
 */
public class RxjobServletInitializer {
   protected SpringApplicationBuilder configure(SpringApplicationBuilder application)
   {
       return application.sources(RxjobApplication.class);
   }
}
