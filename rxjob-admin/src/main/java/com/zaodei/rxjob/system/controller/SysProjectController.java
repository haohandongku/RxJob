package com.zaodei.rxjob.system.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zaodei.rxjob.common.annotation.Log;
import com.zaodei.rxjob.common.enums.BusinessType;
import com.zaodei.rxjob.system.domain.Project;
import com.zaodei.rxjob.system.domain.ProjectUser;
import com.zaodei.rxjob.system.domain.SysUser;
import com.zaodei.rxjob.system.service.IProjectService;
import com.zaodei.rxjob.system.service.ISysUserService;
import com.zaodei.rxjob.core.web.base.BaseController;
import com.zaodei.rxjob.common.page.TableDataInfo;
import com.zaodei.rxjob.common.base.AjaxResult;
import com.zaodei.rxjob.common.utils.ExcelUtil;

/**
 * 项目 信息操作处理
 * 
 * @author wuchanghao
 * @date 2019-02-21
 */
@Controller
@RequestMapping("/system/project")
public class SysProjectController extends BaseController
{
    private String prefix = "system/project";
	
	@Autowired
	private IProjectService projectService;
	
	@Autowired
    private ISysUserService userService;
	
	@RequiresPermissions("system:project:view")
	@GetMapping()
	public String project()
	{
	    return prefix + "/project";
	}
	
	/**
	 * 查询项目列表
	 */
	@RequiresPermissions("system:project:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(Project project)
	{
		startPage();
        List<Project> list = projectService.selectProjectList(project);
		return getDataTable(list);
	}
	
	
	/**
	 * 导出项目列表
	 */
	@RequiresPermissions("system:project:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Project project)
    {
    	List<Project> list = projectService.selectProjectList(project);
        ExcelUtil<Project> util = new ExcelUtil<Project>(Project.class);
        return util.exportExcel(list, "project");
    }
	
	/**
	 * 新增项目
	 */
	@GetMapping("/add")
	public String add()
	{
	    return prefix + "/add";
	}
	
	/**
	 * 新增保存项目
	 */
	@RequiresPermissions("system:project:add")
	@Log(title = "项目", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public AjaxResult addSave(Project project)
	{		
		return toAjax(projectService.insertProject(project));
	}

	/**
	 * 修改项目
	 */
	@GetMapping("/edit/{projectId}")
	public String edit(@PathVariable("projectId") Long projectId, ModelMap mmap)
	{
		Project project = projectService.selectProjectById(projectId);
		mmap.put("project", project);
	    return prefix + "/edit";
	}
	
	/**
	 * 修改保存项目
	 */
	@RequiresPermissions("system:project:edit")
	@Log(title = "项目", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public AjaxResult editSave(Project project)
	{		
		return toAjax(projectService.updateProject(project));
	}
	
	/**
	 * 删除项目
	 */
	@RequiresPermissions("system:project:remove")
	@Log(title = "项目", businessType = BusinessType.DELETE)
	@PostMapping( "/remove/{projectId}")
	@ResponseBody
	public AjaxResult remove(@PathVariable("projectId") Long projectId)
	{		
		return toAjax(projectService.deleteProjectById(projectId));
	}
	
	@RequiresPermissions("system:project:bindUser")
    @GetMapping("/bindUser/{projectId}")
    public String bindUser(@PathVariable("projectId") Long projectId, ModelMap mmap)
    {   
		mmap.addAttribute("projectId", projectId);
        return prefix + "/bindUser";
    }
	
	
	/**
	 * 项目管理-成员管理
	 */
	@RequiresPermissions("system:project:bindUserList")
	@PostMapping("/bindUserList")
	@ResponseBody
	public TableDataInfo bindUserList(Project project)
	{   
		startPage();
		List<Map<String,Object>> list = projectService.selectUsersById(project.getProjectId());
		return getDataTable(list);
	}
	/**
	 * 项目管理-成员管理-删除成员
	 * @param projectId
	 * @param mmap
	 * @return
	 */
	@RequiresPermissions("system:project:deleteUser")
	@PostMapping("/deleteUser/{projectId}")
	@ResponseBody
    public  AjaxResult deleteUser(@PathVariable("projectId") String ids)
    {    
		Long projectId = 0L;
		Long userId = 0L;
		String idStr [] = ids.split("@");
		if(idStr.length<=1){
			return error("错误异常");
		}else{
			projectId =Long.parseLong(idStr[0]);
			userId =Long.parseLong(idStr[1]);
		}
		ProjectUser projectUser = new ProjectUser();
		projectUser.setUserId(userId);
		projectUser.setProjectId(projectId);
		return toAjax(projectService.deleteProjectUser(projectUser));
    }
   
	/**
	 * 项目管理-成员管理-新增成员
	 * @param projectId
	 * @param mmap
	 * @return
	 */
	@RequiresPermissions("system:project:addUser")
    @GetMapping("/addUser/{projectId}")
    public  String addUser(@PathVariable("projectId") Long projectId, ModelMap mmap)
    {    
		mmap.addAttribute("projectId", projectId);
		SysUser user = new SysUser();
		user.setStatus("0");
		List<SysUser> list = userService.selectUserList(user);
		List<Long> userIdlist = projectService.selectUserIdsById(projectId);
		//剔除已经添加成员
		List<SysUser> uList = new ArrayList<SysUser>();
		for (int i = 0; i < list.size(); i++) {
			SysUser sysUser = list.get(i);
			Long userId = sysUser.getUserId();
			if(!userIdlist.contains(userId)){
				uList.add(sysUser);
			}
		}
		mmap.addAttribute("users", uList);
        return prefix + "/addUser";
    }
	
	
	/**
	 * 项目管理-成员管理-新增成员
	 * @param projectId
	 * @param mmap
	 * @return
	 */
	@RequiresPermissions("system:project:addUser")
	@PostMapping("/addUserSave")
	@ResponseBody
    public  AjaxResult addUserSave(ProjectUser projectUser)
    {    
		return toAjax(projectService.insertProjectUser(projectUser));
    }
	
	/**
	 * 项目管理-成员管理-新增成员
	 * @param projectId
	 * @param mmap
	 * @return
	 */
	@RequiresPermissions("system:project:editUser")
    @GetMapping("/editUser/{projectId}")
    public  String editUser(@PathVariable("projectId") String ids, ModelMap mmap)
    {    
		Long projectId = 0L;
		Long userId = 0L;
		String idStr [] = ids.split("@");
		if(idStr.length<=1){
			return prefix + "/editUser";
		}else{
			projectId =Long.parseLong(idStr[0]);
			userId =Long.parseLong(idStr[1]);
		}
		ProjectUser projectUser = new ProjectUser();
		projectUser.setUserId(userId);
		projectUser.setProjectId(projectId);
		Map<String,Object> map = projectService.selectUsersByProjectUser(projectUser);
		SysUser sysUser = userService.selectUserById(userId);
		projectUser.setUserName(sysUser.getUserName() +":"+sysUser.getLoginName());
		projectUser.setRoleId(Long.parseLong(String.valueOf(map.get("roleId"))));
		mmap.addAttribute("projectUser",projectUser);
        return prefix + "/editUser";
    }
	/**
	 * 项目管理-成员管理-新增成员
	 * @param projectId
	 * @param mmap
	 * @return
	 */
	@RequiresPermissions("system:project:addUser")
	@PostMapping("/editUserSave")
	@ResponseBody
    public  AjaxResult editUserSave(ProjectUser projectUser)
    {    
		return toAjax(projectService.updateProjectUser(projectUser));
    }
}
