package com.zaodei.rxjob.system.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zaodei.rxjob.core.web.base.BaseController;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * 
 *@Title:在线用户监控
 *@Description:
 *@Author:wuche@laozo.com
 *@time:2019年1月10日
 */
@Controller
@RequestMapping("/monitorDB")
public class MonitorDBController extends BaseController
{
	private static final Logger logger = LoggerFactory.getLogger(MonitorDBController.class);

	@GetMapping("/monitor.shtm")
	@ResponseBody
    public void monitor() 
    {
		logger.warn("监控接口获取redis值：");
    }
	
}
