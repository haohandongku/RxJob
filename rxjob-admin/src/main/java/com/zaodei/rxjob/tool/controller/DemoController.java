package com.zaodei.rxjob.tool.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zaodei.rxjob.core.web.base.BaseController;
import com.zaodei.rxjob.system.domain.SysOperLog;
import com.zaodei.rxjob.system.service.ISysOperLogService;


/**
 *@Title:
 *@Description:
 *@Author:wuche@laozo.com
 *@time:2019年1月11日
 */
@Controller
@RequestMapping("/demo")
public class DemoController extends BaseController{
    @Autowired
    private ISysOperLogService operLogService;
   
    @GetMapping()
    @ResponseBody
    public SysOperLog selectOperLogById()
    {      
          return operLogService.selectOperLogById(1L);
    }
}
