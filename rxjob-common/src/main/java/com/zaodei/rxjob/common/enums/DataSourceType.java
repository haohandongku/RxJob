package com.zaodei.rxjob.common.enums;


/**
 *@Title:数据源
 *@Description:
 *@Author:wuche@laozo.com
 *@time:2019年1月9日
 */
public enum DataSourceType {
    /**
     * 主库
     */
    MASTER,

    /**
     * 从库
     */
    SLAVE 
}
