package com.zaodei.rxjob.common.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.zaodei.rxjob.common.enums.DataSourceType;


/**
 *@Title:自定义多数据源切换注解
 *@Description:
 *@Author:wuche@laozo.com
 *@time:2019年1月9日
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface DataSource {
    /**
     * 切换数据源名称
     */
    public DataSourceType value() default DataSourceType.MASTER;
}
