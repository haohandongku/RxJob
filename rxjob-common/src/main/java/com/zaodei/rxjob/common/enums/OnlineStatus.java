package com.zaodei.rxjob.common.enums;


/**
 *@Title:用户会话
 *@Description:
 *@Author:wuche@laozo.com
 *@time:2019年1月9日
 */
public enum OnlineStatus {
    /** 用户状态 */
    on_line("在线"), off_line("离线");

    private final String info;

    private OnlineStatus(String info)
    {
        this.info = info;
    }

    public String getInfo()
    {
        return info;
    } 
}
