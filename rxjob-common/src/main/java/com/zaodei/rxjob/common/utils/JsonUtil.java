  
package com.zaodei.rxjob.common.utils;  

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 
 *@Title:JSON工具类
 *@Description:
 *@Author:wuche@laozo.com
 *@time:2019年1月17日
 */
public class JsonUtil {
	private static final Logger log = LoggerFactory.getLogger(JsonUtil.class);
	private final static String EMPTY_JSON = "{}";
	private final static ObjectMapper mapper = new ObjectMapper();
	static{
		mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
		mapper.setSerializationInclusion(Include.NON_NULL);
	}
	public static String toJson(Object obj){
		try {
			return mapper.writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			log.error("", e);
			return EMPTY_JSON;
		}
	}
	@SuppressWarnings("rawtypes")
	public static Map getMapFromJson(String json) throws JsonParseException,
			JsonMappingException, IOException {
		return mapper.readValue(json, Map.class);
	}
   
	@SuppressWarnings("rawtypes")
	public static String getJsonFromMap(Map map)
			throws JsonGenerationException, JsonMappingException, IOException {
		String jsonString = mapper.writeValueAsString(map);
		return jsonString;
	}
	public static List<Map<String,Object>> getJsonFromList(String json)
			throws JsonGenerationException, JsonMappingException, IOException {
		return mapper.readValue(json,new TypeReference<List<Map<String,Object>>>() {});
	}
}
  