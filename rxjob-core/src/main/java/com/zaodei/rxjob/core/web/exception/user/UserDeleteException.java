package com.zaodei.rxjob.core.web.exception.user;

/**
 * 
 *@Title:用户账号已被删除
 *@Description:
 *@Author:wuche@laozo.com
 *@time:2019年1月10日
 */
public class UserDeleteException extends UserException
{
    private static final long serialVersionUID = 1L;

    public UserDeleteException()
    {
        super("user.password.delete", null);
    }
}
