package com.zaodei.rxjob.core.web.exception.user;

/** 
 *@Title:用户不存在异常类
 *@Description:
 *@Author:wuche@laozo.com
 *@time:2019年1月10日
 */
public class UserNotExistsException extends UserException
{
    private static final long serialVersionUID = 1L;

    public UserNotExistsException()
    {
        super("user.not.exists", null);
    }
}
