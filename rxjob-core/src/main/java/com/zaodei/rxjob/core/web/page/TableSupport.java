package com.zaodei.rxjob.core.web.page;

import com.zaodei.rxjob.common.constant.Constants;
import com.zaodei.rxjob.common.page.PageDomain;
import com.zaodei.rxjob.core.util.ServletUtils;


/**
 *@Title:表格数据处理
 *@Description:
 *@Author:wuche@laozo.com
 *@time:2019年1月10日
 */
public class TableSupport {
    /**
     * 封装分页对象
     */
    public static PageDomain getPageDomain()
    {
        PageDomain pageDomain = new PageDomain();
        pageDomain.setPageNum(ServletUtils.getParameterToInt(Constants.PAGE_NUM));
        pageDomain.setPageSize(ServletUtils.getParameterToInt(Constants.PAGE_SIZE));
        pageDomain.setOrderByColumn(ServletUtils.getParameter(Constants.ORDER_BY_COLUMN));
        pageDomain.setIsAsc(ServletUtils.getParameter(Constants.IS_ASC));
        return pageDomain;
    }

    public static PageDomain buildPageRequest()
    {
        return getPageDomain();
    }
}
