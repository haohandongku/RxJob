package com.zaodei.rxjob.core.web.exception.user;

/**
 * 
 *@Title:用户密码不正确或不符合规范异常类
 *@Description:
 *@Author:wuche@laozo.com
 *@time:2019年1月10日
 */
public class UserPasswordNotMatchException extends UserException
{
    private static final long serialVersionUID = 1L;

    public UserPasswordNotMatchException()
    {
        super("user.password.not.match", null);
    }
}
